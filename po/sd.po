# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-01-19 20:04+0000\n"
"PO-Revision-Date: 2023-01-05 11:07+0000\n"
"Last-Translator: Anonymous <noreply@weblate.org>\n"
"Language-Team: Sindhi <https://hosted.weblate.org/projects/lomiri/telephony-"
"service/sd/>\n"
"Language: sd\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 4.15.1-dev\n"

#: indicator/messagingmenu.cpp:409
#, qt-format
msgid "%1 missed call"
msgid_plural "%1 missed calls"
msgstr[0] ""
msgstr[1] ""

#: indicator/messagingmenu.cpp:495
#, qt-format
msgid "%1 voicemail message"
msgid_plural "%1 voicemail messages"
msgstr[0] ""
msgstr[1] ""

#: indicator/metrics.cpp:48
#, qt-format
msgid "<b>%1</b> calls made today"
msgstr ""

#: indicator/metrics.cpp:46
#, qt-format
msgid "<b>%1</b> calls received today"
msgstr ""

#: indicator/metrics.cpp:44
#, qt-format
msgid "<b>%1</b> text messages received today"
msgstr ""

#: indicator/metrics.cpp:42
#, qt-format
msgid "<b>%1</b> text messages sent today"
msgstr ""

#: indicator/textchannelobserver.cpp:482
msgid "AMBER Alert"
msgstr ""

#: approver/approver.cpp:523
msgid "Accept"
msgstr ""

#: indicator/textchannelobserver.cpp:485
msgid "Alert"
msgstr ""

#: indicator/textchannelobserver.cpp:610
#, qt-format
msgid "Attachment: %1 audio clip"
msgid_plural "Attachments: %1 audio clips"
msgstr[0] ""
msgstr[1] ""

#: indicator/textchannelobserver.cpp:608
#, qt-format
msgid "Attachment: %1 contact"
msgid_plural "Attachments: %1 contacts"
msgstr[0] ""
msgstr[1] ""

#: indicator/textchannelobserver.cpp:612
#, qt-format
msgid "Attachment: %1 file"
msgid_plural "Attachments: %1 files"
msgstr[0] ""
msgstr[1] ""

#: indicator/textchannelobserver.cpp:604
#, qt-format
msgid "Attachment: %1 image"
msgid_plural "Attachments: %1 images"
msgstr[0] ""
msgstr[1] ""

#: indicator/textchannelobserver.cpp:606
#, qt-format
msgid "Attachment: %1 video"
msgid_plural "Attachments: %1 videos"
msgstr[0] ""
msgstr[1] ""

#: indicator/authhandler.cpp:72
msgid "Authentication failed. Do you want to review your credentials?"
msgstr ""

#: indicator/messagingmenu.cpp:345
msgid "Call back"
msgstr ""

#: approver/approver.cpp:469 approver/approver.cpp:484
msgid "Caller number is not available"
msgstr ""

#: approver/approver.cpp:481
#, qt-format
msgid "Calling from %1"
msgstr ""

#: approver/approver.cpp:475
msgid "Calling from private number"
msgstr ""

#: approver/approver.cpp:478
msgid "Calling from unknown number"
msgstr ""

#: indicator/ussdindicator.cpp:142
#, fuzzy
msgid "Cancel"
msgstr "Cancel"

#: indicator/textchannelobserver.cpp:227
msgid "Deactivate flight mode and try again from the messaging application."
msgstr ""

#: approver/approver.cpp:545
msgid "Decline"
msgstr ""

#: indicator/textchannelobserver.cpp:468
msgid "Emergency Alert"
msgstr ""

#: indicator/textchannelobserver.cpp:471
msgid "Emergency Alert: Extreme"
msgstr ""

#: indicator/textchannelobserver.cpp:477
msgid "Emergency Alert: Notice"
msgstr ""

#: indicator/textchannelobserver.cpp:474
msgid "Emergency Alert: Severe"
msgstr ""

#: approver/approver.cpp:536
msgid "End + Answer"
msgstr ""

#: approver/approver.cpp:522
msgid "Hold + Answer"
msgstr ""

#: indicator/messagingmenu.cpp:350
msgid "I missed your call - can you call me now?"
msgstr ""

#: indicator/messagingmenu.cpp:353
msgid "I'll be 20 minutes late."
msgstr ""

#: approver/approver.cpp:93
msgid "I'm busy at the moment. I'll call later."
msgstr ""

#: indicator/messagingmenu.cpp:352
msgid "I'm busy at the moment. I'll call you later."
msgstr ""

#: approver/approver.cpp:94
msgid "I'm running late, on my way now."
msgstr ""

#: indicator/messagingmenu.cpp:351
msgid "I'm running late. I'm on my way."
msgstr ""

#: approver/approver.cpp:553
msgid "Message & decline"
msgstr ""

#: indicator/textchannelobserver.cpp:749
#, qt-format
msgid "Message from %1"
msgstr ""

#. TRANSLATORS : %1 is the group name and %2 is the recipient name
#: indicator/messagingmenu.cpp:262 indicator/textchannelobserver.cpp:671
#: indicator/textchannelobserver.cpp:675
#, qt-format
msgid "Message to %1 from %2"
msgstr ""

#. TRANSLATORS : %1 is the recipient name
#: indicator/messagingmenu.cpp:265 indicator/messagingmenu.cpp:269
#: indicator/textchannelobserver.cpp:679 indicator/textchannelobserver.cpp:683
#, qt-format
msgid "Message to group from %1"
msgstr ""

#: indicator/textchannelobserver.cpp:295
msgid "New Group"
msgstr ""

#. TRANSLATORS : message displayed when any error occurred while receiving a MMS (case when cellular-data is off, or any downloading issue). Notify that there was a message, the user can find more about it in the messaging-app.
#: indicator/textchannelobserver.cpp:615
msgid "New MMS message"
msgstr ""

#: indicator/authhandler.cpp:94
msgid "No"
msgstr ""

#: indicator/metrics.cpp:49 indicator/metrics.cpp:51
msgid "No calls made today"
msgstr ""

#: indicator/metrics.cpp:47
msgid "No calls received today"
msgstr ""

#: indicator/metrics.cpp:45
msgid "No text messages received today"
msgstr ""

#: indicator/metrics.cpp:43
msgid "No text messages sent today"
msgstr ""

#: indicator/textchannelobserver.cpp:367 indicator/ussdindicator.cpp:116
msgid "Ok"
msgstr ""

#: approver/approver.cpp:456
#, qt-format
msgid "On [%1]"
msgstr ""

#: indicator/telephony-service-call.desktop.in:3
msgid "Phone Calls"
msgstr ""

#: approver/approver.cpp:95
msgid "Please call me back later."
msgstr ""

#: indicator/textchannelobserver.cpp:910
msgid "Please, select a SIM card:"
msgstr ""

#: approver/approver.cpp:460 indicator/messagingmenu.cpp:414
msgid "Private number"
msgstr ""

#: indicator/ussdindicator.cpp:119
msgid "Reply"
msgstr ""

#: indicator/displaynamesettings.cpp:35
#, qt-format
msgid "SIM %1"
msgstr ""

#: indicator/telephony-service-sms.desktop.in:3
msgid "SMS"
msgstr ""

#: indicator/textchannelobserver.cpp:373
msgid "Save"
msgstr ""

#: indicator/messagingmenu.cpp:151 indicator/messagingmenu.cpp:293
#: indicator/messagingmenu.cpp:360
msgid "Send"
msgstr ""

#: indicator/textchannelobserver.cpp:531
msgid "Show alert"
msgstr ""

#: indicator/messagingmenu.cpp:354
msgid "Sorry, I'm still busy. I'll call you later."
msgstr ""

#: indicator/metrics.cpp:50
#, qt-format
msgid "Spent <b>%1</b> minutes in calls today"
msgstr ""

#: indicator/messagingmenu.cpp:83 indicator/messagingmenu.cpp:87
msgid "Telephony Service"
msgstr ""

#: approver/main.cpp:46
msgid "Telephony Service Approver"
msgstr ""

#: indicator/main.cpp:53
msgid "Telephony Service Indicator"
msgstr ""

#: indicator/textchannelobserver.cpp:236
msgid "The message could not be sent"
msgstr ""

#: indicator/textchannelobserver.cpp:230
msgid "Try again from the messaging application."
msgstr ""

#: approver/approver.cpp:65
msgid "Unknown caller"
msgstr ""

#: approver/approver.cpp:463 indicator/messagingmenu.cpp:244
#: indicator/messagingmenu.cpp:418 indicator/textchannelobserver.cpp:640
msgid "Unknown number"
msgstr ""

#: indicator/textchannelobserver.cpp:225
msgid "Unlock your sim card and try again from the messaging application."
msgstr ""

#: indicator/textchannelobserver.cpp:328
msgid "View Group"
msgstr ""

#: indicator/textchannelobserver.cpp:249 indicator/textchannelobserver.cpp:713
msgid "View message"
msgstr ""

#: indicator/messagingmenu.cpp:500
msgid "Voicemail"
msgstr ""

#: indicator/messagingmenu.cpp:492
msgid "Voicemail messages"
msgstr ""

#: indicator/authhandler.cpp:91
msgid "Yes"
msgstr ""

#: indicator/textchannelobserver.cpp:309
msgid "You joined a new group"
msgstr ""

#. TRANSLATORS : %1 is the group name
#: indicator/textchannelobserver.cpp:306
#, qt-format
msgid "You joined group %1"
msgstr ""

#. TRANSLATORS : %1 is the group name
#: indicator/textchannelobserver.cpp:302
#, qt-format
msgid "You joined group %1 "
msgstr ""
